<?php

namespace Drupal\easy_gallery;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\easy_gallery\Entity\ImageGalleryInterface;

/**
 * Defines the storage handler class for Image gallery entities.
 *
 * This extends the base storage class, adding required special handling for
 * Image gallery entities.
 *
 * @ingroup easy_gallery
 */
class ImageGalleryStorage extends SqlContentEntityStorage implements ImageGalleryStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(ImageGalleryInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {image_gallery_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {image_gallery_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(ImageGalleryInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {image_gallery_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('image_gallery_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
