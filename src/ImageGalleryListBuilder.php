<?php

namespace Drupal\easy_gallery;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Image gallery entities.
 *
 * @ingroup easy_gallery
 */
class ImageGalleryListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Image gallery ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\easy_gallery\Entity\ImageGallery */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.image_gallery.edit_form',
      ['image_gallery' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
