<?php

namespace Drupal\easy_gallery\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Image gallery entities.
 *
 * @ingroup easy_gallery
 */
interface ImageGalleryInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Image gallery name.
   *
   * @return string
   *   Name of the Image gallery.
   */
  public function getName();

  /**
   * Sets the Image gallery name.
   *
   * @param string $name
   *   The Image gallery name.
   *
   * @return \Drupal\easy_gallery\Entity\ImageGalleryInterface
   *   The called Image gallery entity.
   */
  public function setName($name);

  /**
   * Gets the Image gallery creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Image gallery.
   */
  public function getCreatedTime();

  /**
   * Sets the Image gallery creation timestamp.
   *
   * @param int $timestamp
   *   The Image gallery creation timestamp.
   *
   * @return \Drupal\easy_gallery\Entity\ImageGalleryInterface
   *   The called Image gallery entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Image gallery published status indicator.
   *
   * Unpublished Image gallery are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Image gallery is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Image gallery.
   *
   * @param bool $published
   *   TRUE to set this Image gallery to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\easy_gallery\Entity\ImageGalleryInterface
   *   The called Image gallery entity.
   */
  public function setPublished($published);

  /**
   * Gets the Image gallery revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Image gallery revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\easy_gallery\Entity\ImageGalleryInterface
   *   The called Image gallery entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Image gallery revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Image gallery revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\easy_gallery\Entity\ImageGalleryInterface
   *   The called Image gallery entity.
   */
  public function setRevisionUserId($uid);

}
