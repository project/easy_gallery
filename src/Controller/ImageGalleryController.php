<?php

namespace Drupal\easy_gallery\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\easy_gallery\Entity\ImageGalleryInterface;

/**
 * Class ImageGalleryController.
 *
 *  Returns responses for Image gallery routes.
 */
class ImageGalleryController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Image gallery  revision.
   *
   * @param int $image_gallery_revision
   *   The Image gallery  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($image_gallery_revision) {
    $image_gallery = $this->entityManager()->getStorage('image_gallery')->loadRevision($image_gallery_revision);
    $view_builder = $this->entityManager()->getViewBuilder('image_gallery');

    return $view_builder->view($image_gallery);
  }

  /**
   * Page title callback for a Image gallery  revision.
   *
   * @param int $image_gallery_revision
   *   The Image gallery  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($image_gallery_revision) {
    $image_gallery = $this->entityManager()->getStorage('image_gallery')->loadRevision($image_gallery_revision);
    return $this->t('Revision of %title from %date', ['%title' => $image_gallery->label(), '%date' => format_date($image_gallery->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Image gallery .
   *
   * @param \Drupal\easy_gallery\Entity\ImageGalleryInterface $image_gallery
   *   A Image gallery  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(ImageGalleryInterface $image_gallery) {
    $account = $this->currentUser();
    $langcode = $image_gallery->language()->getId();
    $langname = $image_gallery->language()->getName();
    $languages = $image_gallery->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $image_gallery_storage = $this->entityManager()->getStorage('image_gallery');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $image_gallery->label()]) : $this->t('Revisions for %title', ['%title' => $image_gallery->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all image gallery revisions") || $account->hasPermission('administer image gallery entities')));
    $delete_permission = (($account->hasPermission("delete all image gallery revisions") || $account->hasPermission('administer image gallery entities')));

    $rows = [];

    $vids = $image_gallery_storage->revisionIds($image_gallery);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\easy_gallery\ImageGalleryInterface $revision */
      $revision = $image_gallery_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $image_gallery->getRevisionId()) {
          $link = $this->l($date, new Url('entity.image_gallery.revision', ['image_gallery' => $image_gallery->id(), 'image_gallery_revision' => $vid]));
        }
        else {
          $link = $image_gallery->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.image_gallery.translation_revert', ['image_gallery' => $image_gallery->id(), 'image_gallery_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.image_gallery.revision_revert', ['image_gallery' => $image_gallery->id(), 'image_gallery_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.image_gallery.revision_delete', ['image_gallery' => $image_gallery->id(), 'image_gallery_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['image_gallery_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
