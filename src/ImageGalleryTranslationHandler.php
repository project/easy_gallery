<?php

namespace Drupal\easy_gallery;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for image_gallery.
 */
class ImageGalleryTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
