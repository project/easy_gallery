<?php

namespace Drupal\easy_gallery\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Gallery entities.
 *
 * @ingroup easy_gallery
 */
class GalleryDeleteForm extends ContentEntityDeleteForm {


}
