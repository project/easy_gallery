<?php

namespace Drupal\easy_gallery\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Image gallery entities.
 *
 * @ingroup easy_gallery
 */
class ImageGalleryDeleteForm extends ContentEntityDeleteForm {


}
