<?php

namespace Drupal\easy_gallery;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\easy_gallery\Entity\ImageGalleryInterface;

/**
 * Defines the storage handler class for Image gallery entities.
 *
 * This extends the base storage class, adding required special handling for
 * Image gallery entities.
 *
 * @ingroup easy_gallery
 */
interface ImageGalleryStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Image gallery revision IDs for a specific Image gallery.
   *
   * @param \Drupal\easy_gallery\Entity\ImageGalleryInterface $entity
   *   The Image gallery entity.
   *
   * @return int[]
   *   Image gallery revision IDs (in ascending order).
   */
  public function revisionIds(ImageGalleryInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Image gallery author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Image gallery revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\easy_gallery\Entity\ImageGalleryInterface $entity
   *   The Image gallery entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(ImageGalleryInterface $entity);

  /**
   * Unsets the language for all Image gallery with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
