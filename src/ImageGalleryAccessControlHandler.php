<?php

namespace Drupal\easy_gallery;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Image gallery entity.
 *
 * @see \Drupal\easy_gallery\Entity\ImageGallery.
 */
class ImageGalleryAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\easy_gallery\Entity\ImageGalleryInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished image gallery entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published image gallery entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit image gallery entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete image gallery entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add image gallery entities');
  }

}
