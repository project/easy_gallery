<?php

/**
 * @file
 * Contains image_gallery.page.inc.
 *
 * Page callback for Image gallery entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Image gallery templates.
 *
 * Default template: image_gallery.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_image_gallery(array &$variables) {
  // Fetch ImageGallery Entity Object.
  $image_gallery = $variables['elements']['#image_gallery'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
